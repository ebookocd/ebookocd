"""
Copyright © 2020 Ralph Seichter

This file is part of eBookOCD.

eBookOCD is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

eBookOCD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with eBookOCD. If not, see <https://www.gnu.org/licenses/>.
"""
import re

from ebookocd.api import PatternReplacement
from ebookocd.transform.default import DefaultTransformer

""" Mistakes found in Drizzt Do'Urden novels. """
PATTERN_REPLACEMENTS = [
    # Under-dark, Half-ling, et al
    PatternReplacement(re.compile(r'-(dark|ling|moon|wood)'), r'\1'),
    # The Gutbuster
    PatternReplacement(re.compile('bble dorf', re.I), 'bbledorf'),
    # TOC anchors
    PatternReplacement(re.compile(r'<a id="\S*?"></a>', re.I), ''),
    # Selected paragraph styles
    PatternReplacement(re.compile(r'<p class="(non)?indent"', re.I), '<p'),
]


class Drizzt(DefaultTransformer):
    def setup(self) -> None:
        super().setup()
        self.pattern_replacements.extend(PATTERN_REPLACEMENTS.copy())
