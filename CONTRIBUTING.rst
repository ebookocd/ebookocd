Contributing
============

eBookOCD was designed with extensibility in mind, in order to
encourage third-party contributions of transformer classes. I am
currently experimenting with a transformer for a bundle of Drizzt
Do'Urden ebooks which I recently purchased. My hope is that other
people might write transformers for whatever types of ebooks they like
to clean up or otherwise improve.

There is no formal contribution process yet. If you have an idea, or
if you would like to provide a transformer for public consumption,
please submit an issue to get started.
